﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.OrderAggregate
{
    public class ProductItemOrdered
    {
        public ProductItemOrdered()
        {

        }

        public ProductItemOrdered(int productItemId, string productName, string pictureUrl)
        {
            ProductItemId = productItemId;
            ProductName = productName ?? throw new ArgumentNullException(nameof(productName));
            PictureUrl = pictureUrl ?? throw new ArgumentNullException(nameof(pictureUrl));
        }

        public int ProductItemId { get; set; }
        public string ProductName { get; set; }
        public string PictureUrl { get; set; }
    }
}
