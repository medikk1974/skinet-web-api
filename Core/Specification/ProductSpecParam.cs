﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Specification
{
    public class ProductSpecParam
    {
        private const int MaxPgeSize = 50;
        public int PageIndex { get; set; } = 1;
        
        private int _pageSize = 6;
        public int PageSize
        {
            get => _pageSize;
            set => PageSize = (value > MaxPgeSize) ? MaxPgeSize : value;
        }

        public int? BrandId { get; set; }
        public int? TypeId { get; set; }
        public string Sort { get; set; }

        private string _search;
        public string Search
        {
            get
            {
                return _search;
            }
            set
            {
                _search = value.ToLower();
            }
        }
    }
}
