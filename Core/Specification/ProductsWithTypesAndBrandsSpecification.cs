﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Core.Specification
{
    public class ProductsWithTypesAndBrandsSpecification : BaseSpecification<Product>
    {
        public ProductsWithTypesAndBrandsSpecification(ProductSpecParam productParams)
                : base(x =>
                (string.IsNullOrEmpty(productParams.Search) || x.Name == productParams.Search)&&
                (!productParams.BrandId.HasValue || x.ProductBrandId == productParams.BrandId) &&
                (!productParams.TypeId.HasValue || x.ProductTypeId == productParams.TypeId)
                ) // filter -->> може бути дуже великим це э правило а не самий фыльтер якщо значення немаэ поаерни(всі) якщо значення співпало поаерни(ті що співпали) з такою умовою можна жожавати бехліч 
        {
            AddInclude(x => x.ProductType);
            AddInclude(x => x.ProductBrand);

            AddOrderBy(x => x.Name);
            
            ApplayPaging(productParams.PageSize * (productParams.PageIndex - 1), productParams.PageSize);

            if (!string.IsNullOrEmpty(productParams.Sort))
            {
                switch (productParams.Sort)
                {
                    case "priceAsc":
                        AddOrderBy(p => p.Price);
                        break;
                    case "priceDesc":
                        AddOrderByDesign(p => p.Price);
                        break;
                    default:
                        AddOrderBy(n => n.Name);
                        break;
                }
            }
        }

        public ProductsWithTypesAndBrandsSpecification(string sort, int? brandId, int? typeId)
            : base(x => 
                (!brandId.HasValue || x.ProductBrandId == brandId) &&
                (!typeId.HasValue || x.ProductTypeId == typeId)
            ) // filter -->> може бути дуже великим це э правило а не самий фыльтер якщо значення немаэ поаерни(всі) якщо значення співпало поаерни(ті що співпали) з такою умовою можна жожавати бехліч 
        
        {
            AddInclude(x => x.ProductType);
            AddInclude(x => x.ProductBrand);
            AddOrderBy(x => x.Name);
           
            

            if (!string.IsNullOrEmpty(sort)){
              
                switch (sort){
                    
                    case "priceAsc":
                        AddOrderBy(p => p.Price);
                        break;
                    
                    case "priceDesc":
                        AddOrderByDesign(p => p.Price);
                        break;
                    
                    //default:
                    //    AddOrderBy(n => n.Name);
                    //    break;
                }
                    
            }
        }
        public ProductsWithTypesAndBrandsSpecification(int id) : 
        base(prod => prod.Id == id)
        {
            AddInclude(x => x.ProductType);
            AddInclude(x => x.ProductBrand);
        }
    }
}
