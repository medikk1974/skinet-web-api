﻿using Core.OrderAggregate;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Specification
{
    public class OrdersWithItemAndOrderingSpecifications : BaseSpecification<Order>
    {
        public OrdersWithItemAndOrderingSpecifications(string email)
            : base(o => o.BuyerEmail == email)
        {
            AddInclude(o => o.OrderItems);
            AddInclude(o => o.DeliveryMethod);
            AddOrderByDesign(o => o.OrderDate);
        }

        public OrdersWithItemAndOrderingSpecifications(int id, string email)
            : base(o => o.Id == id && o.BuyerEmail == email)
        {
            AddInclude(o => o.OrderItems);
            AddInclude(o => o.DeliveryMethod);
        }
    }
}
