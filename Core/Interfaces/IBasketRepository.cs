﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IBasketRepository
    {
        Task<CustomerBasket> GetBasketAsynk(string basketId);
        Task<CustomerBasket> UpdateBasketAsynk(CustomerBasket basket);
        Task<bool> DeliteBasketAsynk(string bsketId); 

    }
}
