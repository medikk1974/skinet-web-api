﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Entities.Identity
{
    public class AppUser : IdentityUser
    { 
        public string DisplayName { get; set; }
        public Address Adress { get; set; }
    }
}
