﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class CustomerBasket
    {
        public string Id { get; set; }
        public List<BusketItem> Items { get; set; } = new List<BusketItem>();

        public CustomerBasket()
        {

        }
        public CustomerBasket(string id)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
        }   
    }
}
