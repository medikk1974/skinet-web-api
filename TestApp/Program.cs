﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] ints = { 4 ,4 ,4 ,4};

            // Count the even numbers in the array, using a seed value of 0.
            int numEven = ints.Aggregate(2, (total, next) =>
                                                next % 2 == 0 ? total + 1 : total);

            Console.WriteLine("The number of even integers is: {0}", numEven);
        }
    }
}
