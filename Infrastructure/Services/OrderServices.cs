﻿using Core.Entities;
using Core.Interfaces;
using Core.OrderAggregate;
using Core.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class OrderServices : IOrderServices
    {

        private IUnitOfWork _unitOfWork;
        private readonly IBasketRepository _basketRepository;
        public OrderServices(IUnitOfWork unitOfWork, IBasketRepository basketRepository)
        {
            _unitOfWork = unitOfWork;
            _basketRepository = basketRepository;
        }
        public async Task<Order> CreateOrderAsync(string buyerEmail, int deliveruMethodId, string basketId, Address shipingAddress)
        {
            var basket = await _basketRepository.GetBasketAsynk(basketId);
            var orderItems = new List<OrderItem>();

            basket.Items.ForEach(async item => {
                var productItem = await _unitOfWork.Repository<Product>().GetByIdAsynk(item.Id);
               
                var itemOrder = new ProductItemOrdered(productItem.Id, productItem.Name,
                    productItem.PictureUrl);

                var orderItem = new OrderItem(itemOrder, productItem.Price, item.Quantity);
                orderItems.Add(orderItem);
            });


            var deliveryMethods = await _unitOfWork.Repository<DeliveryMethod>().GetByIdAsynk(deliveruMethodId);

            var subtotal = orderItems.Sum(item => item.Price * item.Quantity);
        
            var order = new Order(buyerEmail, shipingAddress, deliveryMethods, orderItems, subtotal);

            _unitOfWork.Repository<Order>().Add(order);

            //save to db
            var resoult = await _unitOfWork.Complete();

            if(resoult <= 0)
            {
                return null;
            }

            await _basketRepository.DeliteBasketAsynk(basketId); 

            return order;
        }

        public async Task<IReadOnlyList<DeliveryMethod>> GetDeliveryMethodsAsync()
        {
            return await _unitOfWork.Repository<DeliveryMethod>().ListAllAsync();
        }

        public async Task<Order> GetOrderByIdAsync(int id, string buyerEmail)
        {
            var spec = new OrdersWithItemAndOrderingSpecifications(id, buyerEmail);

            return await _unitOfWork.Repository<Order>().GetEntityWithSpec(spec);
        }

        public async Task<IReadOnlyList<Order>> GetOrderForUserAsync(string buyerEmail)
        {
            var spec = new OrdersWithItemAndOrderingSpecifications(buyerEmail);

            return await _unitOfWork.Repository<Order>().ListAsync(spec);
        }
    }
}
