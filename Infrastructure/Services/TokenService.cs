﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Core.Entities.Identity;
using Core.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Infrastructure.Services
{
    public class TokenService : ITokenServices
    {
        private readonly SymmetricSecurityKey _symmetricSecurityKey;
        private readonly IConfiguration _configuration;
        public TokenService(IConfiguration configuration)
        {
            _configuration = configuration;
            _symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Token:Key"]));

        }
        public string CreateTokent(AppUser appUser)
        {
            
            var clims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Email, appUser.Email),
                new Claim(JwtRegisteredClaimNames.GivenName, appUser.DisplayName)
            };


            var creads = new SigningCredentials(_symmetricSecurityKey, SecurityAlgorithms.HmacSha512Signature);

            // налаштування токеа які дані шифруються 
            var tokenDeskription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(clims),
                Expires = DateTime.Now.AddDays(10),
                SigningCredentials = creads,
                Issuer = _configuration["Token:Issuer"]
            };
            var tokenHeandler = new JwtSecurityTokenHandler();

            var token =  tokenHeandler.CreateToken(tokenDeskription);

            return tokenHeandler.WriteToken(token);
        }
    }
}
