﻿using Core.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using System.Threading.Tasks;

namespace Infrastructure.Identity 
{ 
    public class AppIdentityDbContextSeed
    {
        public static async Task SeedUsersAsync(UserManager<AppUser> userManager)
        {
            if (!userManager.Users.Any())
            {
                var newUser = new AppUser()
                {
                    DisplayName = "Bob",
                    Email = "bob@test.com",
                    UserName = "bob@test.com",
                    Adress = new Address
                    {
                        FirstName = "bob",
                        LastName = "Bobtutu",
                        Street = "10 the street",
                        State = "NY",
                        Zipcode = "0987"
                    }
                };
                await userManager.CreateAsync(newUser, "Pa$$w0rd");
            }

        }
    }
}
