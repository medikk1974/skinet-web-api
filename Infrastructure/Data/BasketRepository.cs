﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using StackExchange.Redis;

namespace Infrastructure.Data
{
    public class BasketRepository : IBasketRepository
    {
        private readonly IDatabase _database;

        public BasketRepository(ConnectionMultiplexer connectionMultiplexer)
        {
            _database = connectionMultiplexer.GetDatabase();
        }

        public async Task<bool> DeliteBasketAsynk(string bsketId)
        {
            return await _database.KeyDeleteAsync(bsketId);
        }

        
        public async Task<CustomerBasket> GetBasketAsynk(string basketId)
        {
            var data = await _database.StringGetAsync(basketId);
        
            return data.IsNullOrEmpty ? null : JsonSerializer.Deserialize<CustomerBasket>(data);
        }

        public async Task<CustomerBasket> UpdateBasketAsynk(CustomerBasket basket)
        {
            var created = await _database
                .StringSetAsync(basket.Id, JsonSerializer.Serialize(basket), TimeSpan.FromDays(30));
            if (!created)
            {
                return null;
            }
            
            return await GetBasketAsynk(basket.Id);
        }
    }
}
