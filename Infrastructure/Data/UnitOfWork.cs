﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;

namespace Infrastructure.Data
{
    class UnitOfWork : IUnitOfWork
    {
        private Hashtable _repositorys;
        private StoreContext _storeContext;
        public UnitOfWork(StoreContext storeContext)
        {
            _storeContext = storeContext;
            _repositorys = new Hashtable();
        }
        public async Task<int> Complete()
        {
            return await _storeContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _storeContext.Dispose(); 
        }

        public IGenericRepository<Tentity> Repository<Tentity>() where Tentity : BaseEntity
        {
            var type = typeof(Tentity).Name;
            if (!_repositorys.ContainsKey(type))
            {
                var repositoryType = typeof(GenericRepository<>);
                var repositoryInstance = Activator
                    .CreateInstance(repositoryType.MakeGenericType(typeof(Tentity)), _storeContext);

                _repositorys.Add(type, repositoryInstance);
            }

            return (IGenericRepository<Tentity>) _repositorys[type];
        }
    }
}
