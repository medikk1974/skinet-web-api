﻿using API.Dtos;
using API.Errors;
using API.Exstensions;
using AutoMapper;
using Core.Entities.Identity;
using Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class AccountController : BaseApiController
    {

        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly ITokenServices _tokenServices;
        private readonly IMapper _mapper;
        public AccountController(UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager, ITokenServices tokenServices, IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenServices = tokenServices;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<UserDto>> GetCarrentUser()
        {
            var email = HttpContext.User?.Claims?
                .FirstOrDefault(e => e.Type == ClaimTypes.Email).Value;

            var user = await _userManager.FindByEmailAsync(email);

            return new UserDto
            {
                Email = user.Email,
                Token = _tokenServices.CreateTokent(user),
                DisplayName = user.DisplayName
            };

        }

        [HttpGet("emailexists")]
        public async Task<ActionResult<bool>> CheckEmailExistsAsync([FromQuery] string email)
        {
            return await _userManager.FindByEmailAsync(email) != null;
        }

        [HttpGet("address")]
        [Authorize]
        public async Task<ActionResult<AddressDto>> GetCurentUserAddress()
        {
            var user = await _userManager.FindByEmailWithAddressAsync(HttpContext.User);
            return _mapper.Map<Address, AddressDto>(user.Adress);
        }

        [Authorize]
        [HttpPut("address")]
        public async Task<ActionResult<AddressDto>> UpdateUserUdres(AddressDto addressDto)
        {
            var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
           
            user.Adress = _mapper.Map<AddressDto, Address>(addressDto);

            var resoult = await _userManager.UpdateAsync(user);

            if (resoult.Succeeded)
            {
                return Ok(_mapper.Map<Address, AddressDto>(user.Adress));
            }
            else return BadRequest("Problem update User");
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserDto>> Login([FromForm]LoginDto loginDto)
        {
            var user = await _userManager.FindByEmailAsync(loginDto.Email);

            if (user == null)
            {
                return Unauthorized(new ApiResponse(401));
            }

            var result = await _signInManager
                .CheckPasswordSignInAsync(user, loginDto.Password, false);

            if (!result.Succeeded)
            {
                return Unauthorized(new ApiResponse(401));
            }

            return new UserDto
            {
                Email = user.Email,
                Token = _tokenServices.CreateTokent(user),
                DisplayName = user.DisplayName
            };
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserDto>> Registr(RegisterDto register)
        {
            var user = new AppUser
            {
                DisplayName = register.DisplayName,
                Email = register.Email,
                UserName = register.Email
            };

            var result = await _userManager.CreateAsync(user, register.Password);

            if (!result.Succeeded) return BadRequest(new ApiResponse(400));

            return new UserDto
            {
                DisplayName = user.DisplayName,
                Token = _tokenServices.CreateTokent(user),
                Email = user.Email
            };
        }
    }
}
