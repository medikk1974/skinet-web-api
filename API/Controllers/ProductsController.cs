﻿using API.Dtos;
using API.Errors;
using API.Helper;
using AutoMapper;
using Core.Entities;
using Core.Interfaces;
using Core.Specification;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class ProductsController : BaseApiController
    {
        public IGenericRepository<Product> ProductsRepo { get; }
        public IGenericRepository<ProductBrand> ProductBrandRepo { get; }
        public IGenericRepository<ProductType> ProductTypeRepo { get; }
        public IMapper _mapper { get; set; }

        public ProductsController(IMapper mapper, IGenericRepository<Product> productsRepo, IGenericRepository<ProductBrand> productBrandRepo, IGenericRepository<ProductType> productTypeRepo)
        {
            ProductsRepo = productsRepo;
            ProductBrandRepo = productBrandRepo;
            ProductTypeRepo = productTypeRepo;
            _mapper = mapper;
        } 

        [HttpGet]
        public async Task<ActionResult<Pagination<Product>>> GetProducts([FromQuery]ProductSpecParam productParam)
        {
            var spec = new ProductsWithTypesAndBrandsSpecification(productParam);
            var specforcount = new ProductsWithFiltersForCountSpecification(productParam);

            var totalItem = await ProductsRepo.CountAsync(specforcount);
            var products = _mapper.Map<IReadOnlyList<Product>,IReadOnlyList<ProductsToReturnDto>>(await ProductsRepo.ListAsync(spec));
          
            return Ok(new Pagination<ProductsToReturnDto>(productParam.PageIndex, productParam.PageSize, totalItem, products)); ;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            var spec = new ProductsWithTypesAndBrandsSpecification(id);
            var product = _mapper.Map<Product, ProductsToReturnDto>( await ProductsRepo.GetEntityWithSpec(spec));
            if(product == null)
            {
                return NotFound(new ApiResponse(404));
            }

            return Ok(product);
        }

        [HttpGet("brands")]
        public async Task<ActionResult<List<ProductBrand>>> GetBrands()
        {
            return Ok(await ProductBrandRepo.ListAllAsync());
        }

        [HttpGet("types")]
        public async Task<ActionResult<List<ProductType>>> GetProductTypes()
        {
            return Ok(await ProductTypeRepo.ListAllAsync());
        }
    }
}
