﻿using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;

namespace API.Controllers
{
    public class BasketController: BaseApiController
    {
        private readonly IBasketRepository _basketRepository;
        public BasketController(IBasketRepository repository)
        {
            _basketRepository = repository;
        }

        [HttpGet]
        public async Task<ActionResult<CustomerBasket>> GetBasketById(string id)
        {
            var basket = await _basketRepository.GetBasketAsynk(id);
            return Ok(basket ?? new CustomerBasket(id));
        }

        [HttpPost]
        public async Task<ActionResult<CustomerBasket>> UpdateBasket(CustomerBasket basket)
        {
            var updBask = await _basketRepository.UpdateBasketAsynk(basket);

            return Ok(updBask);
        }

        [HttpDelete]
        public async Task<ActionResult<bool>> DeliteBasketAsynk(string id)
        {
            await _basketRepository.DeliteBasketAsynk(id);
            return Ok(true);
        }
    }
}
