﻿using API.Dtos;
using AutoMapper;
using Core.Entities;
using Core.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Helper
{
    public class MapingProfiles: Profile
    {
        public MapingProfiles()
        {
            CreateMap<Product, ProductsToReturnDto>()
                .ForMember(d => d.ProductBrand, o => o.MapFrom(s => s.ProductBrand.Name))
                .ForMember(d => d.ProductType, o => o.MapFrom(s => s.ProductType.Name))
                .ForMember(d => d.PictureUrl, o => o.MapFrom<ProductUrlResolve>());

            CreateMap<Address, AddressDto>();
            CreateMap<AddressDto, Address>();

            CreateMap<AddressDto, Core.OrderAggregate.Address>();
        }
    }
}
