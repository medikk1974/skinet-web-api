﻿using API.Dtos;
using AutoMapper;
using Core.Entities;
using Microsoft.Extensions.Configuration;
using System;

namespace API.Helper
{
    public class ProductUrlResolve : IValueResolver<Product, ProductsToReturnDto, string>
    {
        IConfiguration _configuration;
        public ProductUrlResolve(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string Resolve(Product source, ProductsToReturnDto destination, string destMember, ResolutionContext context)
        {
            if (!string.IsNullOrEmpty(source.PictureUrl))
            {
                return _configuration["ApiUrl"] + source.PictureUrl;
            }

            return null;
        }
    }
}
